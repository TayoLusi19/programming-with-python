# List Project

The purpose of this Python project is to develop a script that enhances understanding and proficiency in manipulating lists and applying conditional logic in Python. Specifically, the script aims to:

```bash
# Define the list
my_list = [1, 2, 2, 4, 4, 5, 6, 8, 10, 13, 22, 35, 52, 83]

# Part 1: Print out elements higher than or equal to 10
print("Elements higher than or equal to 10:")
filtered_elements = [number for number in my_list if number >= 10]
print(filtered_elements)

# Part 2: Instead of printing elements one by one, create a new list and print it
# Note: This has already been accomplished in Part 1 with list comprehension

# Part 3: Ask the user for a number and filter the list based on that
user_input = input("Enter a number: ")

# Ensure user input is a valid integer
try:
    user_number = int(user_input)
    user_filtered_list = [number for number in my_list if number > user_number]
    print(f"Elements higher than {user_number}:")
    print(user_filtered_list)
except ValueError:
    print("Please enter a valid number.")
```

## Key Improvements:
1. List Comprehension: The use of list comprehension makes the creation of the filtered lists more concise and readable.
2. Error Handling: Adding a try-except block around the user input conversion to integer (int(user_input)) improves the robustness of the script by ensuring that it can handle invalid input gracefully.
3. Efficiency: By directly printing the list that is generated through list comprehension, the code is made more efficient and cleaner.

To execute this script:

1. Save the script in a file, for example, filter_list.py.
2. Run the script using Python by executing python filter_list.py in your terminal or command prompt.
3. Follow the prompt and enter a number when asked to see the list filtered based on your input.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/programming-with-python/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
