# Spreadsheets Project

The objective of this Python program is to demonstrate the capability to manipulate spreadsheet data using Python, specifically focusing on reading from and writing to Excel files with the openpyxl module. The program aims to:

1. Read Spreadsheet Data: Extract employee data from an existing Excel spreadsheet, showcasing the ability to navigate and retrieve information from structured data files.

2. Data Manipulation: Process and sort the extracted data based on specific criteria, in this case, the years of experience of the employees, emphasizing the application of sorting algorithms on complex data structures like lists of dictionaries.

3. Create and Write to Spreadsheets: Generate a new Excel spreadsheet file that reflects the sorted data, demonstrating the capability to not only read from but also create and modify Excel files programmatically.

4. Enhance File Handling Skills: Improve file handling skills by working with Excel files, an essential competency in data analysis and automation tasks in Python.

5. Apply Practical Python Skills: Cement understanding of practical Python concepts, including working with external libraries, handling lists and dictionaries, and using sorting functions.

6. Real-world Application: Bridge theoretical knowledge of Python programming with a practical application that is common in business environments, such as handling employee records, thereby preparing participants for real-world Python tasks.

This project serves as a hands-on application for learners to consolidate their Python programming skills, particularly in handling and manipulating data within spreadsheets, an invaluable skill in data analysis, finance, HR, and more. By completing this project, participants will advance their capabilities in Python, making them adept at automating tasks that involve reading from and writing to Excel files.

**sort_employees.py**
```python
import openpyxl
from operator import itemgetter

def read_and_sort_employees(filename):
    # Load the workbook and select the active worksheet
    workbook = openpyxl.load_workbook(filename)
    sheet = workbook.active

    employees = []

    # Iterate over the rows in the sheet and collect employee data
    for row in sheet.iter_rows(min_row=2, values_only=True):
        name, years_of_experience, _, _ = row
        employees.append({"name": name, "years of experience": years_of_experience})

    # Sort the employees list by years of experience in descending order
    sorted_employees = sorted(employees, key=itemgetter("years of experience"), reverse=True)

    return sorted_employees

def create_sorted_employee_sheet(sorted_employees, output_filename):
    # Create a new workbook and select the active worksheet
    new_workbook = openpyxl.Workbook()
    new_sheet = new_workbook.active
    new_sheet.title = "Sorted Employees"

    # Write headers
    new_sheet.append(["Name", "Years of Experience"])

    # Write sorted employee data
    for employee in sorted_employees:
        new_sheet.append([employee["name"], employee["years of experience"]])

    # Save the new workbook to a file
    new_workbook.save(output_filename)

# Main program execution
if __name__ == "__main__":
    input_filename = "employees.xlsx"
    output_filename = "employees_sorted.xlsx"

    sorted_employees = read_and_sort_employees(input_filename)
    create_sorted_employee_sheet(sorted_employees, output_filename)

    print(f"Employee data has been sorted and saved to '{output_filename}'.")
```

## Steps to Run the Program:
1. Ensure the "employees.xlsx" file exists in the same directory as this script, or adjust input_filename to the correct path where your file is located.
2. Run the script using a Python interpreter. This can be done by executing python sort_employees.py in your terminal or command prompt, assuming Python is installed and properly configured in your environment.

## What This Program Does:
- Reads Data: It loads the "employees.xlsx" file, iterating over each row to collect employee names and their years of experience.
- Sorts Data: The collected data is sorted based on years of experience in descending order.
- Creates a New Excel File: It then creates a new Excel file named "employees_sorted.xlsx", where it writes the sorted employee data.
- Saves the File: The new file is saved with the sorted data, effectively completing the task.
This program will help you automate the process of sorting employee data by years of experience, demonstrating practical applications of Python in data processing and manipulation using spreadsheets.



# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/programming-with-python/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
