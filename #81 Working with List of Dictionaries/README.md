# List of Dictionaries Project

The objective of this Python project is to enhance proficiency in managing and manipulating complex data structures, specifically lists of dictionaries, which are commonly used in data processing and storage within software applications. By iterating over a list of employee dictionaries, extracting, and printing specific information (such as name, job, and city), and directly accessing nested dictionary values

```bash
employees = [
    {
        "name": "Tina",
        "age": 30,
        "birthday": "1990-03-10",
        "job": "DevOps Engineer",
        "address": {
            "city": "New York",
            "country": "USA"
        }
    },
    {
        "name": "Tim",
        "age": 35,
        "birthday": "1985-02-21",
        "job": "Developer",
        "address": {
            "city": "Sydney",
            "country": "Australia"
        }
    }
]

# Print out the name, job, and city of each employee using a loop
print("Employee Details:")
for employee in employees:
    name = employee['name']
    job = employee['job']
    city = employee['address']['city']
    print(f"Name: {name}, Job: {job}, City: {city}")
    print("-----------------------")

# Print the country of the second employee in the list by accessing it directly without the loop
country_of_second_employee = employees[1]["address"]["country"]
print(f"Country of the second employee: {country_of_second_employee}")
```

## Key Points:
- This script demonstrates how to iterate over a list of dictionaries to access nested information (like the city within the address dictionary) and print it.
- It also shows how to directly access a specific item in a list of dictionaries to retrieve nested data.
- The code is written to be easily adaptable for any number of employees in the list, making it flexible for various dataset sizes.
This approach to handling a list of dictionaries is fundamental in Python, especially when dealing with JSON data structures, which are common in web development and data science projects.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/programming-with-python/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
