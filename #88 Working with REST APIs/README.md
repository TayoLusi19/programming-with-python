# REST APIs Project

The objective of this Python program is to showcase the practical application of Python in interfacing with RESTful APIs, specifically the GitHub API, to retrieve and process data in real-time. By accomplishing this task, the program aims to:

Demonstrate API Consumption: Illustrate how to use the requests library in Python to make HTTP GET requests to a REST API, in this case, the GitHub API, reinforcing the significance of web APIs in modern software development and data exchange.

Process JSON Data: Parse and handle JSON formatted data returned from the API, emphasizing Python's capability in working with common data formats used in web services.

Iterate Through Data Structures: Navigate through lists and dictionaries to access specific data (repository names and URLs), showcasing essential Python data structure manipulation techniques.

Display Relevant Information: Filter and print specific pieces of information from a larger dataset, demonstrating the ability to extract and present data in a user-friendly format.

Enhance Understanding of Web Services: Enhance the learner's understanding of how web services work, how to interact with them, and the practical applications of such interactions in software projects and data analysis tasks.

Promote Best Practices: Encourage best practices in writing clean, efficient, and effective code that interacts with external services securely and robustly.

This project serves as a hands-on exercise for learners to apply Python programming concepts in a practical scenario involving web APIs, improving their understanding of web technologies and data processing while gaining valuable experience in Python programming. It caters to those looking to deepen their programming skills and understanding of how to programmatically access and utilize web-based services and data.

This program uses the requests library to make HTTP GET requests to the GitHub API.

First, ensure you have the requests library installed in your environment. If not, you can install it by running pip install requests in your terminal.

**github_repos.py**
```python
import requests

def fetch_github_repos(user):
    """
    Fetches all public repositories for a given GitHub user
    and prints the name & URL of each repository.
    
    Parameters:
    - user: A string representing the GitHub username.
    """
    url = f"https://api.github.com/users/{user}/repos"
    response = requests.get(url)
    
    # Check if the request was successful
    if response.status_code == 200:
        repos = response.json()
        for repo in repos:
            print(f"Project Name: {repo['name']}\nProject URL: {repo['html_url']}\n")
    else:
        print("Failed to fetch repositories. Please check the username and try again.")

if __name__ == "__main__":
    user = input("Enter the GitHub username: ")
    fetch_github_repos(user)
```

## How to Run This Program:
1. Open your terminal or command prompt.
2. Navigate to the directory where the github_repos.py file is saved.
3. Run the script by typing python github_repos.py and press enter.
4. When prompted, enter the GitHub username for which you want to fetch the public repositories.

## What This Program Does:
- Prompts User Input: It asks for the GitHub username for which the repositories will be fetched.
- Fetches Repositories: Makes a GET request to the GitHub API for the specified user's public repositories.
- Processes and Prints Data: Iterates through the JSON response, printing the name and URL of each repository.
- Error Handling: Checks the HTTP response status code to ensure the request was successful before attempting to process the data. If the request fails (e.g., due to an incorrect username), it prints an error message.
This program is a practical demonstration of using Python to interact with web APIs, process JSON data, and present information in a clear and concise format.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/programming-with-python/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
