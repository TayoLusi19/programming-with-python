# Python Program 'Calculator' Project

The objective of this Python calculator program project is to design and implement a simple yet robust calculator that can perform basic arithmetic operations—addition, subtraction, multiplication, and division—based on user input.

```bash
def calculator(number1, number2, operation):
    try:
        if operation == "plus":
            return number1 + number2
        elif operation == "minus":
            return number1 - number2
        elif operation == "multiply":
            return number1 * number2
        elif operation == "divide":
            return number1 / number2
    except ZeroDivisionError:
        return "Error: Division by zero is not allowed."

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

number_of_calculations_done = 0
print("Simple Calculator Program")
print("Type 'exit' at any time to quit.")

while True:
    number1 = input("Enter the first number: ")
    if number1.lower() == "exit":
        print(f"You performed {number_of_calculations_done} calculations. Exiting the program.")
        break

    number2 = input("Enter the second number: ")
    if number2.lower() == "exit":
        print(f"You performed {number_of_calculations_done} calculations. Exiting the program.")
        break

    operation = input("What operation do you want to perform? (plus, minus, multiply, divide): ")
    if operation.lower() == "exit":
        print(f"You performed {number_of_calculations_done} calculations. Exiting the program.")
        break

    if not is_number(number1) or not is_number(number2):
        print("Error: Only numbers are allowed.")
        continue

    if operation not in ["plus", "minus", "multiply", "divide"]:
        print("Error: Operation not supported.")
        continue

    result = calculator(float(number1), float(number2), operation)
    print(f"The result is: {result}")
    number_of_calculations_done += 1
```

### Enhancements:
- Improved Validation: Added a function is_number to check if the input can be converted to a float, which handles both integer and decimal inputs.
- Handling Division by Zero: Incorporated a try-except block in the calculator function to catch division by zero errors, providing user-friendly feedback.
- Streamlined Exit Process: Simplified the exit process so that typing "exit" at any prompt ends the program, ensuring a consistent user experience.
- Unified Case Handling: Used .lower() method to ensure case-insensitive comparison for the "exit" command and operation names, improving usability.

### Concepts Covered:
- Working with Different Data Types: Utilizes strings, floats, and integers.
- Conditionals: Uses if-elif-else statements to decide the operation to perform.
- Type Conversion: Converts strings to floats for arithmetic operations.
- User Input and Validation: Checks user input for valid numbers and supported operations.
- Looping: Uses a while loop to keep the program running until the user decides to exit.
- Exception Handling: Catches and handles exceptions to prevent runtime errors from crashing the program.

This enhanced calculator program provides a comprehensive demonstration of handling various programming concepts in Python, making it an excellent exercise for beginners to intermediate learners to solidify their understanding of fundamental programming principles.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/programming-with-python/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
