# Dictionaries Project

This Python project is to demonstrate proficiency in manipulating dictionaries, one of Python's core data structures, to perform various operations such as updating key-value pairs, removing keys, merging multiple dictionaries, and conducting aggregate calculations on dictionary values. 


```bash
# Update job to Software Engineer and remove the age key
employee = {
    "name": "Tim",
    "age": 30,
    "birthday": "1990-03-10",
    "job": "DevOps Engineer"
}

employee["job"] = "Software Engineer"
employee.pop("age")

# Loop through and print key:value pairs
print("Updated Employee Dictionary:")
for key, value in employee.items():
    print(f"{key}: {value}")

# Merge two dictionaries and perform calculations
dict_one = {'a': 100, 'b': 400}
dict_two = {'x': 300, 'y': 200}

# Merging dictionaries
dict_merged = {**dict_one, **dict_two}
print("\nMerged Dictionary:", dict_merged)

# Summing up all values
total_value = sum(dict_merged.values())
print(f"Total Sum of Values: {total_value}")

# Finding min and max values
min_value = min(dict_merged.values())
max_value = max(dict_merged.values())
print(f"Min Value: {min_value}")
print(f"Max Value: {max_value}")
```

## Key Points and Improvements:
1. Merging Dictionaries: The {**dict_one, **dict_two} syntax provides a concise way to merge two dictionaries in Python 3.5+.

2. Summing Values: The sum() function directly calculates the total of dictionary values without needing to loop through each value.

3. Min and Max Values: The min() and max() functions find the smallest and largest values in the dictionary, eliminating the need to sort the values list.

4. Efficiency: This consolidated script reduces redundancy by avoiding the repetition of dictionary definitions and operations, making the code cleaner and easier to maintain.

5. Best Practices: Using direct dictionary and list methods enhances readability and performance.

This script showcases fundamental dictionary manipulation techniques in Python, including updating, removing keys, merging dictionaries, and performing aggregate operations on dictionary values.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/programming-with-python/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
