# Python Program 'Guessing Game' Project

The objective of this Python program, "Guessing Game," is to create an engaging and interactive game where users attempt to guess a randomly generated number within a specified range. This project aims to consolidate and apply fundamental Python programming concepts in a fun and practical context, specifically focusing on:

1. Built-In Module Usage: Utilizing Python's built-in random module to generate random numbers, demonstrating how to incorporate Python’s standard library to extend functionality in applications.

2. User Input Handling: Implementing mechanisms to capture and process user inputs, reinforcing the importance of interactive applications and enhancing user engagement.

3. Conditional Logic: Employing comparison operators within if-elif-else statements to provide conditional responses based on the user's guess relative to the randomly generated number, showcasing the critical role of conditionals in decision-making processes.

4. Loop Constructs: Using a while loop to continuously prompt the user for guesses until the correct number is guessed, illustrating how loops facilitate repeated actions and interactive experiences in programming.

5. Immediate Feedback Mechanism: Providing instant feedback to the user after each guess, indicating whether the guess was too high, too low, or correct, thus emphasizing the significance of feedback in interactive games and applications for improved user experience.

6. Program Termination: Demonstrating the control of program flow by exiting the loop and program upon the correct guess, highlighting techniques for managing application lifecycle and user interactions.

By completing this project, participants will improve their ability to integrate various Python concepts such as loops, conditionals, and the random module in a cohesive and fun application. This game serves as an excellent practice for beginners to apply theoretical knowledge in a simple yet practical programming challenge, enhancing their problem-solving skills, understanding of Python syntax, and ability to create programs that interact dynamically with users.

Here's a simple Python program for the "Guessing Game" that meets the objectives outlined. This program generates a random number between 1 and 9 (inclusive) and runs a loop where the user is asked to guess the number until they get it right. It provides feedback on whether the guess is too high or too low, and congratulates the user upon guessing correctly.

```bash
import random

def guessing_game():
    number_to_guess = random.randint(1, 9)  # Generates a random number between 1 and 9
    number_of_guesses = 0  # Track the number of guesses

    while True:
        try:
            user_guess = input("Guess the number between 1 and 9 (or type 'exit' to quit): ")
            if user_guess.lower() == 'exit':
                print("Game exited. Better luck next time!")
                break

            user_guess = int(user_guess)  # Convert input to an integer
            number_of_guesses += 1  # Increment guess count

            if user_guess < number_to_guess:
                print("You guessed too low.")
            elif user_guess > number_to_guess:
                print("You guessed too high.")
            else:
                print(f"YOU WON! It took you {number_of_guesses} guesses.")
                break  # Exit the loop if the guess is correct

        except ValueError:  # Handle non-integer inputs
            print("Please enter a valid number.")

guessing_game()
```
 
## Program Explanation:
- Random Number Generation: The random.randint(1, 9) function generates a random number between 1 and 9, inclusive.
- User Input: The program uses input() to get the user's guess. It also allows the user to exit the game by typing 'exit'.
- Loop and Feedback: A while loop runs indefinitely until the user guesses the correct number or exits the game. It provides feedback on each guess.
- Error Handling: The try-except block catches ValueError exceptions to handle cases where the user inputs non-numeric values, ensuring the program doesn't crash and provides a message to enter a valid number.
- Guess Count: The program counts the number of guesses taken to find the correct number and displays this count when the user wins.

This game is a fun and straightforward way to practice Python fundamentals, including loops, conditionals, input handling, and using the random module.


# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/programming-with-python/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
