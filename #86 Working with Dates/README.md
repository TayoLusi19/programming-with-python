# Dates Project

The objective of this Python program is to enhance understanding and practical application of Python's datetime module by developing a user-interactive script that calculates the precise time remaining until the user's next birthday. This program aims to accomplish the following educational goals:

Date and Time Manipulation: Demonstrate the ability to manipulate date and time objects in Python, specifically using the datetime module to perform operations such as parsing date strings, comparing dates, and calculating differences.

User Input Processing: Implement robust user input handling, including reading string input from the user, validating this input, and converting it into a date object, thereby reinforcing the importance of data validation and conversion in user-centric applications.

Application of Time Delta Calculations: Utilize timedelta objects to compute the difference between two datetime objects, enabling the calculation of days, hours, and minutes until a future event, in this case, the user's birthday.

Enhanced User Feedback: Provide detailed feedback to the user, not just in terms of days but extending to hours and minutes, offering a more engaging and informative user experience.

Conditional Logic Application: Apply conditional logic to handle different scenarios, such as determining whether the user's next birthday occurs in the current year or the following year, showcasing the practical use of conditionals in decision-making processes within programs.

Practical Implementation of Python Concepts: Cement understanding of core Python concepts—including loops, conditionals, and the datetime module—in a practical, real-world application scenario, bridging the gap between theoretical knowledge and practical application.

By completing this project, participants will gain valuable experience in handling date and time in Python, enhancing their programming skill set, and increasing their ability to develop practical, user-oriented Python applications. This project is tailored for learners looking to deepen their comprehension of Python's date and time manipulation capabilities while creating an interactive and functional program.

**Python Script**
```python
from datetime import datetime

# User inputs their birthday
birthday_string = input("Enter your birthday (example - 20/09/2000): ")

# Convert string to datetime object
birthday_date = datetime.strptime(birthday_string, '%d/%m/%Y').date()
today = datetime.now()

# Calculate next birthday
next_birthday = datetime(today.year, birthday_date.month, birthday_date.day)
if next_birthday < today:
    next_birthday = datetime(today.year + 1, birthday_date.month, birthday_date.day)

# Calculate difference
days_till_birthday = (next_birthday - today).days
hours_till_birthday = (next_birthday - today).seconds // 3600
minutes_till_birthday = ((next_birthday - today).seconds % 3600) // 60

# Print result
print(f"{days_till_birthday} days, {hours_till_birthday} hours, and {minutes_till_birthday} minutes till your birthday.")
```

### How This Works:
- User Input: The user is prompted to enter their birthday, which is read as a string.
- String to Date Conversion: The input string is converted to a date object using strptime with the specified format %d/%m/%Y.
- Calculate Next Birthday: Determines whether the next birthday falls in the current year or the next year by comparing today's date with the birthday of the current year. If the birthday has already passed this year, it calculates the birthday for the next year.
- Calculate Time Until Birthday: The difference between the next birthday and the current time (today) is calculated. The days attribute of the resulting timedelta object gives the days until the birthday. The seconds attribute is used to calculate the hours and minutes remaining.
Print the Result: Finally, it prints out the days, hours, and minutes remaining until the user's next birthday.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/programming-with-python/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
