# Objective Statement for This Python Project
The purpose of this Python project is to demonstrate advanced proficiency in Python programming through the creation, organization, and utilization of functions to process and analyze data within complex data structures and strings. 

```bash
employees = [
    {
        "name": "Tina",
        "age": 30,
        "birthday": "1990-03-10",
        "job": "DevOps Engineer",
        "address": {
            "city": "New York",
            "country": "USA"
        }
    },
    {
        "name": "Tim",
        "age": 35,
        "birthday": "1985-02-21",
        "job": "Developer",
        "address": {
            "city": "Sydney",
            "country": "Australia"
        }
    }
]

# Print out the name, job, and city of each employee using a loop
print("Employee Details:")
for employee in employees:
    name = employee['name']
    job = employee['job']
    city = employee['address']['city']
    print(f"Name: {name}, Job: {job}, City: {city}")
    print("-----------------------")

# Print the country of the second employee in the list by accessing it directly without the loop
country_of_second_employee = employees[1]["address"]["country"]
print(f"Country of the second employee: {country_of_second_employee}")
```


By developing a series of functions to:

1. Identify and Display Information: Extract and print detailed information from a list of dictionaries, such as identifying the youngest employee, showcasing the ability to navigate and analyze nested data structures.

2. String Analysis: Calculate and differentiate between uppercase and lowercase letters within a string, highlighting string manipulation techniques and the application of conditional logic to analyze character properties.

3. List Processing: Filter and display even numbers from a list, reinforcing understanding of list operations, iteration, and the application of mathematical conditions to list elements.



This project further aims to enhance code organization skills by:

- Modularizing Code: Encapsulating functionality within discrete functions to promote code reuse and readability.
- Separating Concerns: Using a helper module to define functions separates the function definitions from their usage, following the principle of separation of concerns.
- Main Script Interaction: Demonstrating how to import and utilize custom modules in Python, simulating real-world application structure where modular codebases are common.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/programming-with-python/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
