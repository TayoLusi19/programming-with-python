# Classes and Objects Project

The purpose of this Python OOP project is to develop a comprehensive understanding of object-oriented programming principles through the creation of a university system simulation. This project aims to:

Implement Class Hierarchies: Design and utilize a class hierarchy that models the university system, encapsulating common attributes and methods in a base Person class and extending it in Student and Professor subclasses, showcasing the power and efficiency of inheritance in OOP.

Manage Complex Relationships: Handle complex relationships between different entities in the university system, such as students attending multiple lectures and professors teaching multiple subjects, demonstrating the ability to model and navigate complex data structures.

Demonstrate Encapsulation: Use encapsulation to bundle data (attributes) and methods that operate on the data into a single unit, or class, ensuring that object state is kept safe from unauthorized access and modification.

Utilize Polymorphism: Apply polymorphism to define methods in the subclasses that have the same name as methods in the parent class, allowing objects of different classes to be treated as objects of a common superclass.

Enhance Data Management Capabilities: Develop methods within classes to dynamically add and remove lectures for both students and professors, improving the program's capability to manage and update data dynamically based on user actions.

Promote Code Reusability: Encourage code reusability through inheritance and modular design, reducing redundancy and making the codebase more maintainable and scalable.

Apply Practical OOP Concepts: Apply OOP concepts to solve a practical problem—managing university data—thereby bridging theoretical knowledge with practical application.

This project serves as a hands-on exercise for learners to apply OOP principles in Python, enhancing their understanding of how classes and objects work together to solve real-world problems. By completing this project, participants will gain valuable experience in designing and implementing class hierarchies, managing complex object relationships, and utilizing OOP principles to create scalable and maintainable code.

This structured approach not only solidifies foundational OOP concepts but also prepares participants for more advanced software development tasks, making it an essential project for anyone looking to deepen their Python programming skills and understanding of object-oriented design.

this is a cohesive Python program that includes a Person class for inheritance, along with Student, Professor, and Lecture classes, showcasing object-oriented programming principles in action.

**person.py**
```python
class Person:
    def __init__(self, first_name, last_name, age):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age

    def print_full_name(self):
        print(f"{self.first_name} {self.last_name}")
```

**student.py**
```python
from person import Person

class Student(Person):
    def __init__(self, first_name, last_name, age, lectures=None):
        super().__init__(first_name, last_name, age)
        self.lectures = lectures if lectures is not None else []

    def list_lectures(self):
        print(f"{self.first_name}'s lectures:")
        for lecture in self.lectures:
            print(f"- {lecture.name}")

    def attend_lecture(self, new_lecture):
        self.lectures.append(new_lecture)
        print(f"{self.first_name} now attends {new_lecture.name}")

    def leave_lecture(self, lecture_name):
        self.lectures = [lecture for lecture in self.lectures if lecture.name != lecture_name]
        print(f"{self.first_name} has left the lecture: {lecture_name}")
```

**professor.py**
```python
from person import Person

class Professor(Person):
    def __init__(self, first_name, last_name, age, lectures=None):
        super().__init__(first_name, last_name, age)
        self.lectures = lectures if lectures is not None else []

    def list_lectures(self):
        print(f"{self.first_name}'s lectures:")
        for lecture in self.lectures:
            print(f"- {lecture.name}")

    def teach_lecture(self, new_lecture):
        self.lectures.append(new_lecture)
        print(f"{self.first_name} now teaches {new_lecture.name}")

    def remove_lecture(self, lecture_name):
        self.lectures = [lecture for lecture in self.lectures if lecture.name != lecture_name]
        print(f"{self.first_name} no longer teaches the lecture: {lecture_name}")
```

**lecture.py**

```python
class Lecture:
    def __init__(self, name, max_students, duration, professors=None):
        self.name = name
        self.max_students = max_students
        self.duration_minutes = duration
        self.professors = professors if professors is not None else []

    def print_name_and_duration(self):
        print(f"Lecture: {self.name} - Duration: {self.duration_minutes} minutes")

    def add_professor(self, new_professor):
        self.professors.append(new_professor)
        print(f"{new_professor.first_name} added to the lecture: {self.name}")
```

**main.py**
```python
from professor import Professor
from student import Student
from lecture import Lecture

# Creating lectures
python_lecture = Lecture("Python Basics", 25, 90)
algorithms_lecture = Lecture("Algorithms 101", 30, 120)

# Creating a professor and assigning lectures
prof_jane = Professor("Jane", "Doe", 45, [python_lecture])
prof_jane.print_full_name()
prof_jane.list_lectures()

# Adding a new lecture to teach
prof_jane.teach_lecture(algorithms_lecture)

# Creating a student and assigning attended lectures
student_bob = Student("Bob", "Smith", 20, [python_lecture])
student_bob.print_full_name()
student_bob.list_lectures()

# Student attends a new lecture
student_bob.attend_lecture(algorithms_lecture)
student_bob.list_lectures()

# Printing lecture details
python_lecture.print_name_and_duration()
algorithms_lecture.print_name_and_duration()
```

This program demonstrates how to create and manipulate objects using classes and inheritance in Python. It covers creating classes with constructors, methods, and properties, and shows how objects interact with each other, such as students attending lectures and professors teaching them.

# Contributions
Contributions to this script are welcome. Please ensure to follow good coding practices and provide a detailed description of your changes or improvements.

# License
[LICENSE.md](https://gitlab.com/TayoLusi19/programming-with-python/-/blob/main/LICENSE.md)

# Author
Adetayo Michael Ibijemilusi
